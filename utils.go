package main

import (
	"errors"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var base36 = []byte{
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
	'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
	'u', 'v', 'w', 'x', 'y', 'z'}

func EncodeBytesAsBytes(b []byte) []byte {
	var bigRadix = big.NewInt(36)
	var bigZero = big.NewInt(0)
	x := new(big.Int)
	x.SetBytes(b)

	answer := make([]byte, 0, len(b)*136/100)
	for x.Cmp(bigZero) > 0 {
		mod := new(big.Int)
		x.DivMod(x, bigRadix, mod)
		answer = append(answer, base36[mod.Int64()])
	}

	// leading zero bytes
	for _, i := range b {
		if i != 0 {
			break
		}
		answer = append(answer, base36[0])
	}

	// reverse
	alen := len(answer)
	for i := 0; i < alen/2; i++ {
		answer[i], answer[alen-1-i] = answer[alen-1-i], answer[i]
	}

	return answer
}

func createID(ip string, date string) (error, string) {
	//take last 3 numbers of client IP, xor it with a key. Just want to make a somewhat unique id that changes daily for users to call each other.
	ip = strings.Replace(ip, ".", "", -1)
	last3 := len(ip) - 3
	ip = ip[last3:len(ip)]
	//load key file - remember to set your own 93 byte key string instead of the default
	key, err := ioutil.ReadFile("key")
	if err != nil {
		return errors.New("can't generate id fo user with " + ip + " ip"), ""
	}
	dateint, _ := strconv.Atoi(date)
	keystring := string(key)
	keystring = keystring[dateint*3-3 : dateint*3] //every day of the month gets a new 3 byte key, based on a key string of at least 93 bytes (3 bytes per day of the month up to 31 days)
	id := ""
	//xor ip with key
	for i := 0; i < len(ip); i++ {
		id += string(ip[i] ^ keystring[i])
	}
	//convert to a number system so ID will render properly
	//return hex.EncodeToString([]byte(id)) //going to use base36 instead of base16 for compactness
	idstring := string(EncodeBytesAsBytes([]byte(id)))
	idstring = idstring[1:len(idstring)]
	return nil, idstring
}

func checkBannedWords(message string) bool {
	for _, filter := range config.Filters {
		if math, _ := regexp.MatchString(filter, message); math {
			return false
		}
	}
	return true
}
func checkBan(ip string) (error, bool) {
	data, err := os.ReadFile("blacklist")
	blacklist := string(data)
	if err != nil {
		return errors.New("can't open blacklist file"), true
	}
	lines := strings.Split(blacklist, "\n")
	for _, line := range lines {
		if ip == line {
			return nil, true
		}
	}
	return nil, false

}

func checkAdmin(ip string) bool {
	log.Println(config.AdminIps)
	for _, line := range config.AdminIps {
		log.Println(line)
		log.Println(ip)
		if ip == line {
			return true
		}
	}
	return false

}
