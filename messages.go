package main

import (
	"encoding/json"
	"log"

	"github.com/gorilla/websocket"
)

var messages []Message

const (
	systemMessage = "system_message"
	userMessage   = "user_message"
	adminMessage  = "admin_message"
)

type Message struct {
	Mode    string
	Content string
}

func sendMessage(ws *websocket.Conn, messageMode string, content string) {
	message := Message{messageMode, content}
	data, err := json.Marshal(message)
	if err != nil {
		log.Println(err)
		return
	}
	if messageMode == systemMessage {
		if ws.WriteMessage(websocket.TextMessage, data) != nil {
			log.Println(err)
		}
		return
	}
	messages = append(messages, message)
	for user, _ := range users.Elements {
		if user.ws.WriteMessage(websocket.TextMessage, data) != nil {
			log.Println(err)
			continue
		}
	}
}
