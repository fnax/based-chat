const socket = new WebSocket(`ws://${window.location.hostname}:${location.port}/ws`);
let ul = document.querySelector("ul")

let form = document.querySelector("form")

form.addEventListener("submit", (event) => {
	event.preventDefault();
	let message = form.elements["message"].value
	socket.send(message)
	form.querySelector("#message").value = ""
})
socket.addEventListener("open", (event) => {
	console.log("Connected to server")
});

socket.addEventListener("message", (event) => {
	//console.log("Message from server ", event.data);
	const message = JSON.parse(event.data);
	createMessage(message.Content, message.Mode)
	

});


const createMessage = (content, mode) => {
	let message = document.createElement("li")
	message.innerText = content 
	message.classList.add(mode)
	ul.appendChild(message)
	ul.scrollTop = ul.scrollHeight

}

addEventListener('load', () => {
	ul.scrollTop = ul.scrollHeight
})
