module codeberg.org/fnax/based-chat/server

go 1.19

require github.com/gorilla/websocket v1.5.0

require github.com/mattn/go-sqlite3 v1.14.15 // indirect
