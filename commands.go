package main

import (
	"os"
)

func banCommand(command []string) error {
	f, err := os.OpenFile("blacklist", os.O_WRONLY|os.O_APPEND, 755)
	if err != nil {
		return err
	}
	for i, name := range command {
		if i != 0 {
			for user, _ := range users.Elements {
				if user.id == name {
					_, err = f.WriteString(user.ip + "\n")
					user.ws.Close()
					if err != nil {
						return err
					}
				}
				f.Sync()
			}

		}
	}
	f.Close()
	return nil
}
