// Based chat v1.0.0
package main

import (
	"flag"
	"log"
	"net/http"
	"strconv"
)

var config Config

func main() {
	configFile := flag.String("config-file", "config.json", "define path to config file")
	flag.Parse()
	config = parseConfig(*configFile)
	setupServer()
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(config.ServerPort), nil))
}
