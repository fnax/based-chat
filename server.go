package main

import (
	"html/template"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

func setupServer() {
	fs := http.FileServer(http.Dir("./res"))
	http.Handle("/res/", http.StripPrefix("/res/", fs))
	indexTmpl = template.Must(template.ParseFiles("templates/index.tmpl"))
	http.HandleFunc("/", indexEndpoint)
	users = newUserSet()
	messages = make([]Message, 0)
	http.HandleFunc("/ws", wsEndpoint)
}

var indexTmpl *template.Template

func indexEndpoint(w http.ResponseWriter, r *http.Request) {
	err := indexTmpl.Execute(w, messages)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError)
		return
	}

}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
	var ip string
	if config.ReverseProxy {
		ip = r.Header.Get("X-Real-IP")
	} else {
		ip = r.RemoteAddr[0:strings.LastIndex(r.RemoteAddr, ":")]
	}
	thetime := time.Now()
	err, id := createID(ip, thetime.Format("02"))
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError)
		return
	}
	err, isBanned := checkBan(ip)
	if isBanned {
		http.Error(w, http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError)
		return
	}
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin:     func(r *http.Request) bool { return true },
	}
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError),
			http.StatusInternalServerError)
		return

	}
	user := newUser(ws, id, ip)
	users.Add(user)
	log.Println("Client connected to server from " + ip + " ip")
	defer ws.Close()
	defer users.Remove(user)
	reader(ws, id, ip)

}

func reader(ws *websocket.Conn, id string, ip string) {
	for {
		_, p, err := ws.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		rawMessage := string(p)
		if len(rawMessage) >= 2000 || strings.TrimSpace(rawMessage) == "" || !checkBannedWords(rawMessage) {
			continue
		}
		hasAdmin := checkAdmin(ip)
		if strings.HasPrefix(rawMessage, "/") && hasAdmin {
			command := strings.Split(rawMessage, " ")
			switch command[0] {
			case "/ban":
				err := banCommand(command)
				if err != nil {
					log.Println(err)
					continue
				}
			}
			continue
		}
		var messageMode string
		if hasAdmin {
			messageMode = adminMessage
		} else {
			messageMode = userMessage
		}
		content := id + ": " + rawMessage
		sendMessage(ws, messageMode, content)

	}
}
