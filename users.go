package main

import (
	"errors"

	"github.com/gorilla/websocket"
)

type User struct {
	ws *websocket.Conn
	id string
	ip string
}

func newUser(ws *websocket.Conn, id string, ip string) *User {
	u := User{ws, id, ip}
	return &u
}

type userSet struct {
	Elements map[*User]interface{}
}

func newUserSet() *userSet {
	var s userSet
	s.Elements = make(map[*User]interface{})
	return &s
}

func (s *userSet) Add(user *User) {
	s.Elements[user] = struct{}{}
}

func (s *userSet) Remove(user *User) error {
	if _, exists := s.Elements[user]; !exists {
		return errors.New("element not present in set")
	}
	delete(s.Elements, user)
	return nil
}

var users *userSet
