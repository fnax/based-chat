package main

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	ServerPort             int      `json:"server_port"`
	ReverseProxy           bool     `json:"reverse_proxy"`
	MessagesBeforeCooldown int      `json:"messages_before_cooldown"`
	CooldownTime           int      `json:"cooldown_time"`
	WelcomeMessage         string   `json:"welcome_message"`
	GoodbyeMessage         string   `json:"goodbye_message"`
	AdminIps               []string `json:"admin_ips"`
	Filters                []string `json:"filters"`
}

func parseConfig(configFile string) Config {
	var config Config
	f, err := os.ReadFile(configFile)
	if err != nil {
		log.Fatal("Could not open " + configFile + " file")
	}
	err = json.Unmarshal(f, &config)
	if err != nil {
		//panic(err)
		log.Fatal("Could not parse " + configFile + " file")
	}
	if config.ServerPort == 0 {
		config.ServerPort = 80
	}

	return config
}
